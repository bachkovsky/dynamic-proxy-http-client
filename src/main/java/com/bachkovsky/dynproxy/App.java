package com.bachkovsky.dynproxy;

import com.bachkovsky.dynproxy.api.GoogleSearchApi;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * App demo
 */
@SpringBootApplication
public class App implements CommandLineRunner {
	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	private final GoogleSearchApi api;

	public App(GoogleSearchApi api) {
		this.api = api;
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

	@Override
	@SneakyThrows
	public void run(String... args) {
		LOG.info("Main page status: " + api.mainPageStatus());
		LOG.info("Main page request: " + api.mainPageRequest());
		LOG.info("Doodle search status: " + api.searchDoodleStatus("tesla", "en"));
		try (CloseableHttpResponse response = api.searchSomething("qweqwe")) {
			LOG.info("Search result " + response);
		}
	}
}

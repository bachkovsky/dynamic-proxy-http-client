package com.bachkovsky.dynproxy.api;

import com.bachkovsky.dynproxy.lib.http.uri.Uri;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Service;

/**
 * Some Google requests
 */
@Service
public interface GoogleSearchApi {
	/**
	 * @return http status code for Google main page
	 */
	@Uri("https://www.google.com")
	int mainPageStatus();

	/**
	 * @return request object for Google main page
	 */
	@Uri("https://www.google.com")
	HttpGet mainPageRequest();

	/**
	 * @param query search query
	 * @return result of search request execution
	 */
	@Uri("https://www.google.com/search?q={query}")
	CloseableHttpResponse searchSomething(String query);

	/**
	 * @param query    doodle search query
	 * @param language doodle search language
	 * @return http status code for doodle search result
	 */
	@Uri("https://www.google.com/doodles/?q={query}&hl={language}")
	int searchDoodleStatus(String query, String language);
}

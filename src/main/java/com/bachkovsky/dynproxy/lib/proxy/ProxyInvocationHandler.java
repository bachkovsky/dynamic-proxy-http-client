package com.bachkovsky.dynproxy.lib.proxy;

import java.lang.reflect.InvocationHandler;

/**
 * Dynamic Proxy interface
 */
public interface ProxyInvocationHandler extends InvocationHandler, HandlerMatcher {
}

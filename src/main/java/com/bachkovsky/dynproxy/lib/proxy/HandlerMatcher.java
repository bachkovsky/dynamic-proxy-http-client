package com.bachkovsky.dynproxy.lib.proxy;

import java.lang.reflect.Method;

/**
 * Used for indication of ability to handle method by some handler
 */
public interface HandlerMatcher {
	/**
	 * @return {@code true} if handler is able to handle given method, {@code false} othervise
	 */
	boolean canHandle(Method method);
}

package com.bachkovsky.dynproxy.lib.proxy;

import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Top level dynamic proxy invocation handler, which finds correct implementation based and uses it for method
 * invocation
 */
@Component
public class DynamicProxyInvocationHandlerDispatcher implements InvocationHandler {

	private final List<ProxyInvocationHandler> proxyHandlers;

	/**
	 * @param proxyHandlers all dynamic proxy handlers found in app context
	 */
	public DynamicProxyInvocationHandlerDispatcher(List<ProxyInvocationHandler> proxyHandlers) {
		this.proxyHandlers = proxyHandlers;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) {
		switch (method.getName()) {
			// three Object class methods don't have default implementation after creation with Proxy::newProxyInstance
			case "hashCode":
				return System.identityHashCode(proxy);
			case "toString":
				return proxy.getClass() + "@" + System.identityHashCode(proxy);
			case "equals":
				return proxy == args[0];
			default:
				return doInvoke(proxy, method, args);
		}
	}

	@SneakyThrows
	private Object doInvoke(Object proxy, Method method, Object[] args) {
		return findHandler(method).invoke(proxy, method, args);
	}

	private ProxyInvocationHandler findHandler(Method method) {
		return proxyHandlers.stream()
		                    .filter(h -> h.canHandle(method))
		                    .findAny()
		                    .orElseThrow(() -> new IllegalStateException("No handler was found for method: " +
			                    method));
	}
}

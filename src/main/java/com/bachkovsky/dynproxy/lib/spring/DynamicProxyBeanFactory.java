package com.bachkovsky.dynproxy.lib.spring;

import com.bachkovsky.dynproxy.lib.proxy.DynamicProxyInvocationHandlerDispatcher;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;

import static com.bachkovsky.dynproxy.lib.spring.DynamicProxyBeanFactory.DYNAMIC_PROXY_BEAN_FACTORY;

/**
 * Dynamic proxy instance creation factory
 */
@Component(DYNAMIC_PROXY_BEAN_FACTORY)
public class DynamicProxyBeanFactory {
	public static final String DYNAMIC_PROXY_BEAN_FACTORY = "repositoryProxyBeanFactory";

	private final DynamicProxyInvocationHandlerDispatcher proxy;

	public DynamicProxyBeanFactory(DynamicProxyInvocationHandlerDispatcher proxy) {
		this.proxy = proxy;
	}

	@SuppressWarnings("unused")
	public <T> T createDynamicProxyBean(Class<T> beanClass) {
		//noinspection unchecked
		return (T) Proxy.newProxyInstance(beanClass.getClassLoader(), new Class[]{beanClass}, proxy);
	}
}

package com.bachkovsky.dynproxy.lib.spring;

import lombok.SneakyThrows;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;


/**
 * Component which dynamically creates BeanDefinitions for dynamic proxy beans
 */
@Component
public class DynamicProxyBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
	private static final String[] SCAN_PACKAGES = {"com"};

	private final InterfaceScanner classpathScanner;

	public DynamicProxyBeanDefinitionRegistryPostProcessor() {
		classpathScanner = new InterfaceScanner();
		classpathScanner.addIncludeFilter(new AnnotationTypeFilter(Service.class));
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		for (String basePackage : SCAN_PACKAGES) {
			createRepositoryProxies(basePackage, registry);
		}
	}

	@SneakyThrows
	private void createRepositoryProxies(String basePackage, BeanDefinitionRegistry registry) {
		for (BeanDefinition beanDefinition : classpathScanner.findCandidateComponents(basePackage)) {
			Class<?> clazz = Class.forName(beanDefinition.getBeanClassName());
			BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(clazz);
			builder.addConstructorArgValue(clazz);

			builder.setFactoryMethodOnBean(
				"createDynamicProxyBean",
				DynamicProxyBeanFactory.DYNAMIC_PROXY_BEAN_FACTORY
			);
			registry.registerBeanDefinition(ClassUtils.getShortNameAsProperty(clazz), builder.getBeanDefinition());
		}
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

	}

	private static class InterfaceScanner extends ClassPathScanningCandidateComponentProvider {

		InterfaceScanner() {
			super(false);
		}

		@Override
		protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
			return beanDefinition.getMetadata().isInterface();
		}
	}
}

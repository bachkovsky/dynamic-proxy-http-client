package com.bachkovsky.dynproxy.lib.http.handlers;

import com.bachkovsky.dynproxy.lib.http.HttpClient;
import com.bachkovsky.dynproxy.lib.http.uri.UriHandler;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Handler which executes request and returns http status code
 */
@Component
public class HttpCodeInvocationHandler extends HttpInvocationHandler {

	public HttpCodeInvocationHandler(HttpClient client, UriHandler uriHandler) {
		super(client, uriHandler);
	}

	@Override
	@SneakyThrows
	public Integer invoke(Object proxy, Method method, Object[] args) {
		try (CloseableHttpResponse resp = client.execute(new HttpGet(getUri(method, args)))) {
			return resp.getStatusLine().getStatusCode();
		}
	}

	@Override
	public boolean canHandle(Method method) {
		return super.canHandle(method) && method.getReturnType().equals(int.class);
	}
}

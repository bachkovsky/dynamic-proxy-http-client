package com.bachkovsky.dynproxy.lib.http;

import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

/**
 * Simple Apache HttpClient wrapper
 * Makes some {@link CloseableHttpClient} methods accessible to other application parts via injection
 */
@Service
public class HttpClient {

	private final CloseableHttpClient httpClient;

	public HttpClient() {
		httpClient = HttpClients.createDefault();
	}

	@SneakyThrows
	public CloseableHttpResponse execute(HttpUriRequest request) {
		return httpClient.execute(request);
	}

	@PreDestroy
	@SneakyThrows
	public void shutdown() {
		httpClient.close();
	}
}

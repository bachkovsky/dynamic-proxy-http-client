package com.bachkovsky.dynproxy.lib.http.handlers;

import com.bachkovsky.dynproxy.lib.http.HttpClient;
import com.bachkovsky.dynproxy.lib.http.uri.UriHandler;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Handler which constructs HttpGet request and does not execute it
 */
@Component
public class HttpGetRequestInvocationHandler extends HttpInvocationHandler {
	public HttpGetRequestInvocationHandler(HttpClient client, UriHandler uriHandler) {
		super(client, uriHandler);
	}

	@Override
	public HttpGet invoke(Object proxy, Method method, Object[] args) {
		return new HttpGet(getUri(method, args));
	}

	@Override
	public boolean canHandle(Method method) {
		return super.canHandle(method) && method.getReturnType().isAssignableFrom(HttpGet.class);
	}
}

package com.bachkovsky.dynproxy.lib.http.uri;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Specifies URI for resource to make request
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Uri {
	String value();
}

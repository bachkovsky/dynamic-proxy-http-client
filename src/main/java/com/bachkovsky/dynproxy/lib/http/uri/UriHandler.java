package com.bachkovsky.dynproxy.lib.http.uri;

import com.bachkovsky.dynproxy.lib.proxy.HandlerMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.PropertyPlaceholderHelper;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toMap;

/**
 * Method handler with capabilities of URI processing
 * Handles methods annotated with {@link Uri}, replaces parameter placeholders in URI strings with actual values.
 */
@Component
public class UriHandler implements HandlerMatcher {

	private final PropertyPlaceholderHelper placeholderReplacer;

	public UriHandler() {
		placeholderReplacer = new PropertyPlaceholderHelper("{", "}");
	}

	@Override
	public boolean canHandle(Method method) {
		return method.isAnnotationPresent(Uri.class);
	}

	/**
	 * @param method method with {@link Uri} annotation
	 * @param args   method parameters
	 * @return URI string with substituted placeholder parameters
	 */
	public String getUriString(Method method, Object[] args) {
		String uri = method.getAnnotation(Uri.class).value();
		Parameter[] parameters = method.getParameters();
		Map<String, String> params = IntStream.range(0, parameters.length)
		                                      .boxed()
		                                      .collect(toMap(
			                                      i -> parameters[i].getName(),
			                                      i -> args[i].toString(),
			                                      (a, b) -> b
		                                      ));
		return placeholderReplacer.replacePlaceholders(uri, params::get);
	}
}

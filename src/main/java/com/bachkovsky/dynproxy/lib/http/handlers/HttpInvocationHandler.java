package com.bachkovsky.dynproxy.lib.http.handlers;

import com.bachkovsky.dynproxy.lib.http.HttpClient;
import com.bachkovsky.dynproxy.lib.http.uri.UriHandler;
import com.bachkovsky.dynproxy.lib.proxy.ProxyInvocationHandler;

import java.lang.reflect.Method;

/**
 * Abstract invocation handler for Http request handlers
 */
public abstract class HttpInvocationHandler implements ProxyInvocationHandler {
	final HttpClient client;
	private final UriHandler uriHandler;

	HttpInvocationHandler(HttpClient client, UriHandler uriHandler) {
		this.client = client;
		this.uriHandler = uriHandler;
	}

	@Override
	public boolean canHandle(Method method) {
		return uriHandler.canHandle(method);
	}

	final String getUri(Method method, Object[] args) {
		return uriHandler.getUriString(method, args);
	}
}

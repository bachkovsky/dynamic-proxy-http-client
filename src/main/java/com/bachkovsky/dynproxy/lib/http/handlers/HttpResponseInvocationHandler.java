package com.bachkovsky.dynproxy.lib.http.handlers;

import com.bachkovsky.dynproxy.lib.http.HttpClient;
import com.bachkovsky.dynproxy.lib.http.uri.UriHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Handler which executes request and returns {@link CloseableHttpResponse} object
 */
@Component
public class HttpResponseInvocationHandler extends HttpInvocationHandler {
	public HttpResponseInvocationHandler(HttpClient client, UriHandler uriHandler) {
		super(client, uriHandler);
	}

	@Override
	public CloseableHttpResponse invoke(Object proxy, Method method, Object[] args) {
		return client.execute(new HttpGet(getUri(method, args)));
	}

	@Override
	public boolean canHandle(Method method) {
		return super.canHandle(method) && method.getReturnType().isAssignableFrom(CloseableHttpResponse.class);
	}
}
